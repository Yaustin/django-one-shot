from django.shortcuts import get_object_or_404, redirect, render
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):  # views grads info from queryset to database
    todolist = TodoList.objects.all()
    context = {
        "todo_list_list": todolist,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todolist = TodoList.objects.get(id=id)
    context = {
        "todolist": todolist
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "todo_list_list": todolist,
        "form": form
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todolist_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist_instance.delete()
        return redirect(todo_list_list)

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect(todo_list_detail, id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos_items/create.html", context)


def todo_item_update(request, id):
    todoitem_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem_instance)
        if form.is_valid():
            todoitem_instance = form.save()
            return redirect(todo_list_detail, id=todoitem_instance.list.id)
    else:
        form = TodoItemForm(instance=todoitem_instance)
    context = {
        "form": form
    }

    return render(request, "todos_items/edit.html", context)
